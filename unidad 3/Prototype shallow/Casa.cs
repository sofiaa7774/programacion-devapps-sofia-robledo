﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype_shallow
{
    public class Casa : ICloneable
    {
        public int Recamaras { get; set; }
        public string Direccion { get; set; }
        public void Resguardar()
        {
            Console.WriteLine("*Resguardando*");
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
