﻿using System;
using static System.Console;

namespace Prototype_shallow
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Prototype shallow o prototype superficial es un patron de diseño que pertenece al tipo de CREACION, basicamente" +
                      "Sirve para clonar objetos correctamente (funciona correctamente si el objeto que queremos clonar no tiene como propiedades" +
                      " otros objetos)");

            Casa casa = new Casa() { Direccion = "Parque industrial #12", Recamaras = 20 };
            WriteLine($"Recamaras de la primera casa {casa.Recamaras}");
            Casa segundaCasa = casa;
            segundaCasa.Recamaras = 40;
            WriteLine($"Recamaras de la segunda casa {segundaCasa.Recamaras}");
            WriteLine("Pero al no clonarlo correctamente utilizando este patron, sucede que se liga a la primera casa");
            WriteLine($"Recamaras de la primera casa {casa.Recamaras}");

            WriteLine("Ahora vamos a clonar el objeto con el patron y sobreescribirlo");
            segundaCasa = (Casa)casa.Clone();
            segundaCasa.Recamaras = 42;
            WriteLine($"Recamaras de la primera casa {casa.Recamaras}");
            WriteLine($"Recamaras de la segunda casa {segundaCasa.Recamaras}");

            Console.ReadKey();
        }
    }
}
