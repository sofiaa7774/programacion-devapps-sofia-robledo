﻿using System;

namespace Mediador
{
    class Program
    {
        static void Main(string[] args)
        {
            IMediador oMediador = new Mediador();

            User oUsuario = new UserCommon(oMediador);
            User oAdministrador = new UserAdmin(oMediador);

            oMediador.Add(oUsuario);
            oMediador.Add(oAdministrador);

            oUsuario.Comunicar("Hola administrador, soy un nuevo usuario", oAdministrador);
            oAdministrador.Comunicar("Hola usuario, soy el administrador", oUsuario);
            Console.ReadKey();
        }
       
    }
    
}
