﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mediador
{
    public class UserAdmin : User
    {
        public UserAdmin(IMediador mediador) : base(mediador)
        {
        }

        public override void Recieve(string message, User origen)
        {
            Console.WriteLine($"Nuevo mensaje para Administrador: {message}");
        }
    }
}
