﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mediador
{
    public interface IMediador
    {
        void Add(User user);
        void Send(string Message, User origen, User Destino);
    }
}
