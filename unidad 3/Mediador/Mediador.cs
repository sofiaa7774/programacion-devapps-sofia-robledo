﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace Mediador
{
    public class Mediador : IMediador
    {
        public List<User> users;
        public Mediador()
        {
            users = new List<User>();
        }
        public void Add(User user)
        {
            users.Add(user);
        }

        public void Send(string Message, User origen, User Destino)
        {
            var user = users[users.IndexOf(Destino)];
            user.Recieve(Message, origen);
        }
    }
}
