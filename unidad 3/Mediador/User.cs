﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mediador
{
    public abstract class User
    {
        private IMediador mediador;
        public IMediador Mediador
        {
            get;
        }

        public User(IMediador mediador)
        {
            this.mediador = mediador;
        }

        public void Comunicar(string message, User _oDestino)
        {
            this.mediador.Send(message,this, _oDestino);
        }
        public abstract void Recieve(string message, User origen);
    }
}
