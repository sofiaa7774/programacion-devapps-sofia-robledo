﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mediador
{
    public class UserCommon : User
    {
        public UserCommon(IMediador mediador) : base(mediador)
        {
        }

        public override void Recieve(string message, User origen)
        {
            Console.WriteLine($"Nuevo mensaje para Usuario Comun: {message}");
        }
    }
}
