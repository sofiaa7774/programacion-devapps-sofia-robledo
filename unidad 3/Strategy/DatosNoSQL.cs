﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy
{
    class DatosNoSQL : ICrud
    {
        public void Crear()
        {
            Console.WriteLine("Crear desde NoSQL");
        }

        public void Delete()
        {
            Console.WriteLine("Eliminar desde NoSQL");
        }

        public void Read()
        {
            Console.WriteLine("Leer desde NoSQL");
        }

        public void Update()
        {
            Console.WriteLine("Actualizar desde NoSQL");
        }
    }
}
