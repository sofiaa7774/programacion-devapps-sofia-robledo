﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy
{
    interface ICrud
    {
        void Crear();
        void Read();
        void Update();
        void Delete();

    }
}
