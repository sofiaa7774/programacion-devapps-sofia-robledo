﻿using System;

namespace Strategy
{
    class Program
    {
        static void Main(string[] args)
        {
            CapaNegocios oCapaNegocios = new CapaNegocios(enumDestino.SQL);

            oCapaNegocios.Crear();
            oCapaNegocios.Delete();
            oCapaNegocios.Update();
            oCapaNegocios.Read();
            Console.ReadKey();
        }
    }
}
