﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy
{
    public class DatosSQL : ICrud
    {
        public void Crear()
        {
            Console.WriteLine("Crear desde SQL");
        }

        public void Delete()
        {
            Console.WriteLine("Eliminar desde SQL");
        }

        public void Read()
        {
            Console.WriteLine("Leer desde SQL");
        }

        public void Update()
        {
            Console.WriteLine("Actualizar desde SQL");
        }
    }
}
