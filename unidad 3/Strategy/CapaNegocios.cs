﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy
{
    class CapaNegocios
    {
        private ICrud oDatos;
        public CapaNegocios()
        {

        }
        public CapaNegocios(enumDestino destino)
        {
            switch (destino)
            {
                case enumDestino.SQL:
                    oDatos = new DatosSQL();
                    break;
                case enumDestino.NOSQL:
                    oDatos = new DatosNoSQL();
                    break;
                default:
                    break;
            }
        }

        public void Crear()
        {
            oDatos.Crear();
        }

        public void Delete()
        {
            oDatos.Delete();
        }

        public void Read()
        {
            oDatos.Read();
        }

        public void Update()
        {
            oDatos.Update();
        }
    }
}
