﻿using InyeccionDependencias;
using System;
using static System.Console;

namespace InyeccionDependenciasv
{
    class Program
    {
        static void Main(string[] args)
        {
            IProducto oNieveVanilla = new Nieve("Vanilla");
            
            var oCafeteria = new Cafeteria(oNieveVanilla);

            oCafeteria.Preparar();
            WriteLine("Se preparo el producto correctamente.");
            ReadKey();
        }
    }
}
