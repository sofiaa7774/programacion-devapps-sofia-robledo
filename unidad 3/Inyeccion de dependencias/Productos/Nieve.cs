﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InyeccionDependencias
{
    public class Nieve : IProducto
    {
        public string sabor { get; set; }
        public Nieve(string sabor)
        {
            this.sabor = sabor;
        }
        public void Preparar()
        {
            Console.WriteLine($"Estoy preparando una nieve de {sabor}");
        }
    }
}
