﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InyeccionDependencias
{
    public class Capuccino : IProducto
    {
        public void Preparar()
        {
            Console.WriteLine("Estoy preparando un capuccino");
        }
    }
}
