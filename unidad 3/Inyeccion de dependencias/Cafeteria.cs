﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InyeccionDependencias
{
    public class Cafeteria
    {
        private IProducto producto;
        public Cafeteria(IProducto producto)
        {
            this.producto = producto;
        }

        public void Preparar()
        {
            producto.Preparar();
        }
    }
}
