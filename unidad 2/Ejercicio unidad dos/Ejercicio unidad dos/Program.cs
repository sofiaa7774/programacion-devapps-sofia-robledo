﻿using System;
using System.Dynamic;
namespace Ejercicio_unidad_dos
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ejemplo de Interfaz, clase abstracta y polimorfismo");
            Celular[] Celulares = new Celular[2];

            Android android = new Android("Huawei","XAS ||","Camara HD",8000);
            android.Llamar();

            Iphone Iphone = new Iphone("Iphone", "10XS", "Camara 8K", 20000);
            Iphone.Llamar();

            Celulares[0] = android;
            Celulares[1] = Iphone;
            Console.WriteLine("Fin.");

            Console.ReadKey();
            
        }
    }

    abstract class Celular
    {
        public abstract double Costo { get; set; }
        public abstract string Camara { get; set; }
        public abstract void Llamar();
    }

    class Android : Celular, ICelularBase
    {
        public override double Costo { get; set; }
        public override string Camara { get; set; }
        public string Numero { get; set; }
        public string Modelo { get; set; }

        public Android(string _Numero,string _Modelo, string _Camara, double _Costo)
        {
            Numero = _Numero;
            Modelo = _Modelo;
            Camara = _Camara;
            Costo = _Costo;
        }

        public override void Llamar()
        {
            Console.WriteLine("Estoy llamando desde mi celular android");
        }
    }
    class Iphone : Celular, ICelularBase
    {
        public override double Costo { get; set; }
        public override string Camara { get; set; }
        public string Numero { get; set; }
        public string Modelo { get; set; }

        public Iphone(string _Numero, string _Modelo, string _Camara, double _Costo)
        {
            Numero = _Numero;
            Modelo = _Modelo;
            Camara = _Camara;
            Costo = _Costo;
        }

        public override void Llamar()
        {
            Console.WriteLine("Estoy llamando desde mi celular iphone");
        }
    }

    public interface ICelularBase
    {
        string Numero { get; set; }
        string Modelo { get; set; }
    }
}

