﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using unidad_dos.Modelos;

namespace unidad_dos
{
    public abstract class CRUD
    {
        public abstract bool Crear(string nombre, string descripcion, double costo);
        public abstract List<Producto> Leer();
        public abstract bool Eliminar(int id);
        public abstract bool Actualizar(Producto prod);
    }
}
