﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using unidad_dos.Modelos;

namespace unidad_dos
{
    public partial class Frmactualizaroañadir2 : Form
    {
        int id;
        string[] Datos;
        public Frmactualizaroañadir2()
        {
            InitializeComponent();
        }
        public Frmactualizaroañadir2(string Titulo, int id = 0, string[] _Datos = null)
        {
            InitializeComponent();
            this.Datos = _Datos;
            this.id = id;
            Text = Titulo;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (id != 0)
            {
                Datos CapaDatos = new Datos();
                Producto prod = new Producto();
                prod.Id = this.id;
                prod.Nombre = textBox1.Text;
                prod.Descripcion = textBox2.Text;
                prod.Costo = double.Parse(textBox3.Text);
                CapaDatos.Actualizar(prod);
            }
            else
            {
                Datos CapaDatos = new Datos();
                CapaDatos.Crear(textBox1.Text, textBox2.Text, double.Parse(textBox3.Text));
            }
            this.DialogResult = DialogResult.OK;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frmactualizaroañadir2_Load(object sender, EventArgs e)
        {
            if (this.Datos != null)
            {
                textBox1.Text = this.Datos[1];
                textBox2.Text = this.Datos[2];
                textBox3.Text = this.Datos[3];
            }
        }
    }
}
