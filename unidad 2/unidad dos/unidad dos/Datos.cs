﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using unidad_dos.Modelos;

namespace unidad_dos
{
    class Datos : CRUD
    {
        MySqlConnection conn;
        public Datos()
        {
            string connStr="Server=localhost;Database=productos; Uid=root;password=; SslMode=None;";
           
            conn = new MySqlConnection(connStr);
        }
        public override bool Crear(string nombre, string descripcion, double costo)
        {
            try
            {
                using (conn)
                {
                    conn.Open();
                    string sql = $"INSERT INTO Productos(Nombre,Descripcion,Costo) VALUES ('{nombre}','{descripcion}',{costo});";
                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                    int SeEjecuto = cmd.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error En la base de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }

        public override bool Eliminar(int id)
        {
            try
            {
                using (conn)
                {
                    conn.Open();
                    string sql = $"DELETE FROM Productos Where Id = {id};";
                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                    int SeEjecuto = cmd.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error En la base de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }

        public override List<Producto> Leer()
        {
            List<Producto> ListaProductos = new List<Producto>();
            try
            {
                using (conn)
                {
                    conn.Open();
                    string sql = $"Select * from Productos;";
                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                    MySqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        var prod = new Producto();
                        prod.Id = int.Parse(rdr[0].ToString());
                        prod.Nombre = rdr[1].ToString();
                        prod.Descripcion = rdr[2].ToString();
                        prod.Costo = double.Parse(rdr[3].ToString());
                        ListaProductos.Add(prod);
                    }
                    rdr.Close();
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error En la base de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return ListaProductos;
        }

        public override bool Actualizar(Producto prod)
        {
            try
            {
                using (conn)
                {
                    conn.Open();
                    string sql = $"UPDATE Productos SET Nombre = '{prod.Nombre}', Descripcion = '{prod.Descripcion}', costo = {prod.Costo} WHERE Id = {prod.Id};";
                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                    int SeEjecuto = cmd.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error En la base de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }
    }
}

