﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace unidad_dos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void actualizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void añadirRegistroToolStripMenuItem_Click(object sender, EventArgs e)
        {
           Frmactualizaroañadir2 nuevoOActualizar = new Frmactualizaroañadir2("Nuevo");
            if (nuevoOActualizar.ShowDialog(this) == DialogResult.OK)
            {
                nuevoOActualizar.Close();
                cargar();
            }
        }

        private void borrarRegistroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Datos CapaDatos = new Datos();
            CapaDatos.Eliminar(int.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString()));
            cargar();
        }

        private void actualizarRegistroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string[] data = new string[4];
            data[1] = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            data[2] = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            data[3] = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();

            Frmactualizaroañadir2 nuevoOActualizar = new Frmactualizaroañadir2("Actualizar", int.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString()), data);
            if (nuevoOActualizar.ShowDialog(this) == DialogResult.OK)
            {
                nuevoOActualizar.Close();
                cargar();
            }
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        void cargar()
        {
            Datos CapaDatos = new Datos();
            var Productos = CapaDatos.Leer();
            dataGridView1.DataSource = Productos;
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cargar();
        }
    }
}
