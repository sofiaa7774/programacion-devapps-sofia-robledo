﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ejercicio_15
{
    class Program
    {
        static void Main(string[] args)
        {
            //Leemos el archivo que queramos modificar
            string[] lines = File.ReadAllLines(@"C:\Users\Sofia\Desktop\Carpetaa\nota.txt");
            List<string> listaMod = new List<string>();

            string path = @"C:\Users\Sofia\Desktop\Carpetaa\notados.txt";


            // Escribimos un archivo con los valores modificados
            using (var sw = File.CreateText(path))
            {
                foreach (string line in lines)
                {
                    sw.WriteLine(line);
                }
            }
            Console.WriteLine("Se escribio corrrectamente.");

        }
    }
}
