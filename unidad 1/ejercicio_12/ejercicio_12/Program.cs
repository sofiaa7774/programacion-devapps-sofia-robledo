﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace ejercicio_12
{
    class Program
    {
        static void Main(string[] args)
        {
            Electronico cel = new Electronico("Celular Samsumg", "Chico\"");
            cel.television();
        }
    }
    class Electronico
    {
        public Electronico(string _Modelo, string _Tamaño)
        {
            this.Modelo = _Modelo;
            this.Tamaño = _Tamaño;
            WriteLine("\n*El objeto ya ha sido instanciado*\n");
        }

        private string Modelo { get; set; }
        private string Tamaño { get; set; }

        public void television()
        {
            WriteLine("\n*Entrega celular*\n");
        }
    }
}
