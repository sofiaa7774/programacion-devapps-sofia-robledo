﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace ejercicio_13
{
    class Program
    {
        static void Main(string[] args)
        {
            var franquicia = new Franquicia("Cafficrepas", 2);
            franquicia.VenderCafe();
            franquicia.HacerCafe();
        }
        class Cafeteria
        {
            public string Nombre { get; set; }
            public int antiguedad { get; set; }

            public void VenderCafe()
            {
                WriteLine("\n*Vender cafe*");
            }

            public void HacerCafe()
            {
                WriteLine("\n*Hacer cafe*");
            }
        }

        class Franquicia : Cafeteria
        {

            public Franquicia(string _Nombre, int _antiguedad)
            {
                this.Nombre = _Nombre;
                this.antiguedad = _antiguedad;
                WriteLine("\n*Se ha creado con exito*");
            }
            public void Capacitar()
            {
                WriteLine("\n Capacitar...");
            }
        }
    }
}
